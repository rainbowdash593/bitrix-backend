#!/usr/bin/env python3
from os import environ
from app import app

if __name__ == '__main__':
    environ['mode'] = 'dev'
    app.run(host='192.168.255.201', port=5000, debug=True, use_reloader=True, threaded=True)

