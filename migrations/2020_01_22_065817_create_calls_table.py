from orator.migrations import Migration


class CreateCallsTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('calls') as table:
            table.increments('id')
            table.big_integer('phone_id')
            table.foreign('phone_id').references('id').on('phones')
            table.big_integer('delivery_id')
            table.foreign('delivery_id').references('id').on('deliveries')
            table.big_integer('related_id').nullable()
            table.string('outgoing_phone')
            table.string('status')
            table.string('cost').nullable()
            table.string('hangup_cause').nullable()
            table.string('record_file_path').nullable()
            table.string('ivr_answers').nullable()
            table.timestamp('started_at').nullable()
            table.timestamp('answered_at').nullable()
            table.timestamp('manager_started_at').nullable()
            table.timestamp('manager_answered_at').nullable()
            table.timestamp('finished_at').nullable()
            table.timestamp('planned_at').nullable()
            table.timestamp('created_at')
            table.timestamp('updated_at').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('calls')
