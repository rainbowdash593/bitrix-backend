from orator.migrations import Migration


class CreateUsersTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('users') as table:
            table.increments('id')
            table.string('login', 255).unique()
            table.string('email', 255).nullable()
            table.text('password')
            table.text('uuid').unique()
            table.timestamp('created_at')
            table.timestamp('updated_at')
            table.text('zvonobot_login').nullable()
            table.text('zvonobot_password').nullable()
            table.text('bitrix_key').nullable()
            table.text('bitrix_subdomain').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('users')
