from orator.migrations import Migration


class CreateDeliveriesTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('deliveries') as table:
            table.increments('id')
            table.string('name')
            table.string('status')
            table.string('outgoing_phone').nullable()
            table.string('template_id').nullable()
            table.timestamp('created_at')

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('deliveries')
