from orator.migrations import Migration


class CreatePhonesTable(Migration):

    def up(self):
        """
        Run the migrations.
        """
        with self.schema.create('phones') as table:
            table.increments('id')
            table.string('phone')
            table.json('additional_info').nullable()
            table.timestamp('created_at')
            table.timestamp('deleted_at').nullable()

    def down(self):
        """
        Revert the migrations.
        """
        self.schema.drop_if_exists('phones')
