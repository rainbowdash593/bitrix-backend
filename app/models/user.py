import requests
from os import environ
from app import builder
from app.exceptions.user import *
from app.exceptions.zvonobot import *

from app.utils.utils import debug

from app.models import Model
from orator.orm import accessor


class User(Model):

    __table__ = 'users'

    __timestamps__ = ['created_at', 'updated_at']

    __hidden__ = ['password']

    __appends__= ['zvonobot_token', 'zvonobot_api_key']

    __fillable__ = [
        'email',
        'password',
        'login',
        'uuid',
        'zvonobot_login',
        'zvonobot_password',
        'bitrix_key'
    ]

    def get_jwt_identity(self):
        return {
            'id': self.id,
            'uuid': self.uuid,
            'login': self.login,
            'password': self.password,
            'role': 'client'
        }

    @accessor
    def zvonobot_token(self):
        token = builder.cache.get('{user_id}_zvonobot_token'.format(user_id=self.id))
        if not token:
            data = self.zvonobot_auth()
            token = data.get('token')
        return token

    @accessor
    def zvonobot_api_key(self):
        api_key = builder.cache.get('{user_id}_zvonobot_api_key'.format(user_id=self.id))
        if not api_key:
            data = self.zvonobot_auth()
            api_key = data.get('apiKey')
        return api_key

    def zvonobot_auth(self):
        if not self.zvonobot_login or not self.zvonobot_password:
            return {}

        params = {
            'email': self.zvonobot_login,
            'password': self.zvonobot_password,
        }
        request = requests.post(environ.get('F1GOLOS_URL') + '/api/users/authAmo', params)
        resp = request.json()['data']

        debug(resp)

        builder.cache.set('{user_id}_zvonobot_api_key'.format(user_id=self.id), resp['apiKey'], expire=24 * 60 * 60)
        builder.cache.set('{user_id}_zvonobot_token'.format(user_id=self.id), resp['token'], expire=24 * 60 * 60)

        return resp