from app.models import Model
from orator.orm import has_many


class Phone(Model):

    __table__ = 'phones'

    __timestamps__ = False

    __fillable__ = [
        'phone',
        'additional_info',
        'created_at'
    ]

    __casts__ = {
        'additional_info': 'dict'
    }

    @has_many('phone_id')
    def calls(self):
        from app.models.call import Call
        return Call
