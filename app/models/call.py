from app.models import Model
from orator.orm import belongs_to


class Call(Model):

    __table__ = 'calls'

    __timestamps__ = ['created_at', 'updated_at']

    __fillable__ = [
        'phone_id',
        'delivery_id',
        'related_id',
        'outgoing_phone',
        'status',
        'cost',
        'record_file_path',
        'hangup_cause',
        'ivr_answers',
        'started_at',
        'answered_at',
        'manager_started_at',
        'manager_answered_at',
        'finished_at',
        'planned_at',
        'created_at',
        'updated_at',
    ]

    @belongs_to('phone_id')
    def phone(self):
        from app.models.phone import Phone
        return Phone

    @belongs_to('delivery_id')
    def delivery(self):
        from app.models.delivery import Delivery
        return Delivery
