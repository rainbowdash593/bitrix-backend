from app.models import Model
from orator.orm import has_many, accessor


class Delivery(Model):

    __table__ = 'deliveries'

    __timestamps__ = False

    __fillable__ = [
        'name',
        'created_at',
        'status',
        'outgoing_phone',
        'template_id',
    ]

    __appends__ = ['statistics']

    __hidden__ = ['calls']

    STATUSES = ['created', 'in_process', 'stop', 'finished', 'error']


    @has_many('delivery_id')
    def calls(self):
        from app.models.call import Call
        return Call

    @property
    def stat(self):
        return {
            'all': self.calls.count(),
            'finished': self.calls.where('status', 'finished').count()
        }

    @accessor
    def statistics(self):
        return self.stat

