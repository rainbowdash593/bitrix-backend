class UserAlreadyRegisteredException(Exception): pass

class UserNotFoundException(Exception): pass

class WrongCredentialsException(Exception): pass

class InvalidUserPasswordException(Exception): pass
