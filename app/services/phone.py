import io
import csv
import json
import phonenumbers
from phonenumbers import NumberParseException

from app.utils.utils import debug

class PhoneService:

    @staticmethod
    def validate_phone(phone, locale=None):
        try:
            parsed_phone = phonenumbers.parse('+' + str(phone), locale)

            if phonenumbers.is_valid_number(parsed_phone):
                return phone
            else:
                return None
        except NumberParseException as e:
            return None

    @staticmethod
    def preview(file):
        rows = []
        stream = io.StringIO(file.read().decode("UTF8"), newline=None)
        reader = csv.reader(stream, delimiter=';')
        for index, row in enumerate(reader):
            if index > 4:
                break
            rows.append(row)

        return rows

    @staticmethod
    def parse_additional_columns(row, column_mapping):
        additional_info = {}
        if 'phone' in column_mapping:
            del column_mapping['phone']
        for key in column_mapping.keys():
            column = column_mapping[key][0]
            additional_info[key] = row[int(column)]
        return additional_info


    @staticmethod
    def extract_phones(file, mapping):
        column_mapping = {}
        for key, value in json.loads(mapping).items():
            if value in column_mapping:
                column_mapping[value].append(key)
            else:
                column_mapping[value] = [key]

        phone_columns = column_mapping['phone']

        stream = io.StringIO(file.read().decode("UTF8"), newline=None)
        reader = csv.reader(stream, delimiter=';')

        phones = []
        for row in reader:
            for phone_column in phone_columns:
                phone = PhoneService.validate_phone(row[int(phone_column)], 'RU')
                if phone:
                    phones.append({
                        'phone': phone,
                        'additional_columns': PhoneService.parse_additional_columns(row, column_mapping)
                    })

        return phones

