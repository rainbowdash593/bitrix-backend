import logging
import requests
from os import environ
from app import builder
from app.models.user import User
from app.exceptions.user import *
from app.exceptions.zvonobot import *

from app.utils.utils import debug

from flask_jwt_extended import (create_access_token, create_refresh_token, get_jwt_identity)

class AuthService:

    @staticmethod
    def auth(data):
        """
        Auth user

        :param data: user payload
        :return: User, access_token, refresh_token
        """
        login = data.get('login')
        password = data.get('password')

        if not login or not password:
            raise WrongCredentialsException

        user = User.where('login', login).first()

        if not user:
            raise UserNotFoundException

        if not builder.bcrypt.check_password_hash(user.password, password):
            raise InvalidUserPasswordException

        access_token = create_access_token(identity=user.get_jwt_identity())
        refresh_token = create_refresh_token(identity=user.get_jwt_identity())

        return user, access_token, refresh_token

    @staticmethod
    def auth_user():
        """
        Get authenticated user

        :return: User
        """
        user_uuid = get_jwt_identity()['uuid']
        return User.where('uuid', user_uuid).first_or_fail()

    @staticmethod
    def refresh():
        """
        Refresh JWT Token

        :return: token
        """
        current_user = get_jwt_identity()
        ret = {
            'token': create_access_token(identity=current_user)
        }
        return ret

