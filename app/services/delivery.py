import logging
from datetime import datetime
from app.models.call import Call
from app.models.phone import Phone
from app.models.delivery import Delivery
from app.services.phone import PhoneService

from app.utils.utils import debug


class DeliveryService:

    @staticmethod
    def create(form, file):
        phones = PhoneService.extract_phones(file, form['mapping'])
        debug(phones)
        delivery = Delivery.create(name=form.get('name'),
                                   outgoing_phone=form.get('outgoing_phone'),
                                   status='created',
                                   template_id=form.get('template_id'),
                                   created_at=datetime.now())
        for phone in phones:
            try:
                inserted_phone = Phone.create(phone=str(phone['phone']),
                                              additional_info=phone['additional_columns'],
                                              created_at=datetime.now())
                call = Call()
                call.outgoing_phone = form.get('outgoing_phone')
                call.status = 'created'
                call.created_at = datetime.now()
                call.updated_at = datetime.now()
                call.phone().associate(inserted_phone)
                call.delivery().associate(delivery)
                call.save()
            except Exception as e:
                logging.error('Failed to create call: {e}'.format(e=e))

        return delivery
