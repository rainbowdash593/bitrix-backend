import logging
import requests
from os import environ
from flask import Blueprint, request, jsonify
from app.models.user import User

from app.services.phone import PhoneService
from app.services.auth import AuthService
from app.utils.validation import schema_validation
from flask_jwt_extended import jwt_required
from app.utils.utils import debug

ZvonobotController = Blueprint('zvonobot', __name__)


@ZvonobotController.route('/outgoing-phones', methods=['GET'])
@jwt_required
def phones():
    user = AuthService.auth_user()
    params = {'apiKey': user.zvonobot_api_key, 'all': 0}
    try:
        request = requests.post(environ.get('F1GOLOS_URL') + '/apiCalls/getPhones', params)
        phones = request.json()['data']
    except Exception as e:
        logging.error('Failed to parse csv for preview action: {e}'.format(e=e))
        return jsonify({'message': 'Unhandled error'}), 400

    return jsonify({'outgoing_phones': phones}), 200
