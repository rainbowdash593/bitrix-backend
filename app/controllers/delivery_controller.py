import logging
from flask import Blueprint, request, jsonify
from app.models.user import User
from app.models.delivery import Delivery
from app.services.phone import PhoneService
from app.services.delivery import DeliveryService
from app.utils.validation import schema_validation
from flask_jwt_extended import jwt_required
from app.utils.utils import debug

DeliveryController = Blueprint('delivery', __name__)

#TODO validate request
@DeliveryController.route('/base-preview', methods=['POST'])
@jwt_required
def preview():
    file = request.files['file']
    try:
        rows = PhoneService.preview(file)
    except Exception as e:
        logging.error('Failed to parse csv for preview action: {e}'.format(e=e))
        return jsonify({'message': 'Unhandled error'}), 400

    return jsonify({'rows': rows}), 200

#TODO validate request
@DeliveryController.route('/create', methods=['POST'])
@jwt_required
def create():
    form = request.form.to_dict(flat=True)
    file = request.files['file']

    try:
        delivery = DeliveryService.create(form, file)
    except Exception as e:
        logging.error('Failed to create delivery: {e}'.format(e=e))
        return jsonify({'message': 'Unhandled error'}), 400

    return jsonify({'delivery': delivery.serialize()}), 200

@DeliveryController.route('/get', methods=['GET'])
@jwt_required
def get():
    deliveries = Delivery.get()
    return jsonify({'deliveries': deliveries.serialize()})

@DeliveryController.route('/info/<id>', methods=['GET'])
@jwt_required
def info(id):
    delivery = Delivery.find_or_fail(int(id))

    return jsonify({'delivery': delivery.serialize()})

@DeliveryController.route('/calls/<id>', methods=['GET'])
@jwt_required
def calls(id):
    data = request.values.to_dict()
    debug(data)
    delivery = Delivery.find_or_fail(int(id))
    calls = delivery.calls().with_('phone').paginate(int(data.get('items', 10)), int(data.get('page', 1)))

    return jsonify({'total': calls.total, 'calls': calls.to_dict()})