import logging
from flask import Blueprint, request, jsonify
from app.models.user import User

from app.exceptions.user import *
from app.schemas.user import UserSchemas
from app.services.auth import AuthService
from app.utils.validation import schema_validation
from flask_jwt_extended import jwt_required

UserController = Blueprint('user', __name__)
schemas = UserSchemas()


@UserController.route('/login', methods=['POST'])
@schema_validation(schemas.auth)
def auth():
    data = request.get_json(force=True)

    try:
        user, access_token, refresh_token = AuthService.auth(data)
    except UserNotFoundException:
         return jsonify({'message': 'User not found'}), 400
    except WrongCredentialsException:
        return jsonify({'message': 'Invalid login or password'}), 401
    except Exception as e:
        logging.error('Failed to auth user: {e}'.format(e=e))
        return jsonify({'message': 'Unhandled error'}), 400

    return jsonify({'user': user.serialize(), 'access_token': access_token, 'refresh_token': refresh_token}), 200


@UserController.route('/refresh', methods=['POST'])
@jwt_required
def refresh():
    refresh_token = AuthService.refresh()
    return jsonify(refresh_token), 200


@UserController.route('/auth-user', methods=['GET'])
@jwt_required
def protect():
    user = AuthService.auth_user()
    return jsonify({'user': user.serialize()}), 200


@UserController.route('/update', methods=['POST'])
@jwt_required
def update():
    data = request.get_json(force=True)

    user = User.find_or_fail(data.get('id'))
    user.zvonobot_login = data.get('zvonobot_login')
    user.zvonobot_password = data.get('zvonobot_password')
    user.bitrix_key = data.get('bitrix_key')
    user.bitrix_subdomain = data.get('bitrix_subdomain')
    user.save()

    return jsonify({'user': user.serialize()}), 200
