from app.controllers.user_controller import UserController
from app.controllers.delivery_controller import DeliveryController
from app.controllers.zvonobot_controller import ZvonobotController

ROUTES = [
    {
        'prefix': '/api',
        'group': [
            {'prefix': '/user', 'controller':  UserController},
            {'prefix': '/deliveries', 'controller':  DeliveryController},
            {'prefix': '/zvonobot', 'controller':  ZvonobotController},
        ]
    }
]
