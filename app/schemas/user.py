

class UserSchemas:

    @property
    def registration(self):
        return {
            'phone': {'required': True, 'type': 'string', 'minlength': 10},
            'email': {'required': True, 'type': 'string', 'empty': False},
            'name': {'required': True, 'type': 'string', 'empty': False},
            'surname': {'required': True, 'type': 'string', 'empty': False},
            'second_name': {'required': True, 'type': 'string', 'empty': False},
            'address': {'required': True, 'type': 'string', 'empty': False},
            'birthday': {'required': True, 'type': 'string', 'empty': False},
        }
    
    @property
    def auth(self):
        return {
            'login': {'required': True, 'type': 'string', 'empty': False},
            'password': {'required': True, 'type': 'string'},

        }
