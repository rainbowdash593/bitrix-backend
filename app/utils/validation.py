from functools import wraps
from flask import request, jsonify
from cerberus import Validator

def schema_validation(schema):
    def decorator(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            if request.get_json():
                data = request.get_json()
            else:
                data = request.values.to_dict()
            v = Validator(schema)
            v.allow_unknown = True
            if v.validate(data):
                return function(*args, **kwargs)
            else:
                return jsonify({'errors': v.errors}), 400

        return wrapper
    return decorator