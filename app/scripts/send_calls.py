import time
from os import path, environ
from app.models.call import Call
from app.utils.log import log_setup

if __name__ == '__main__':
    log_setup(filename=path.basename(__file__).split('.')[0] + '.log', log_level='DEBUG')

    while True:
        calls = Call.where('status', 'created').limit(50).get()
        #TODO send calls
        time.sleep(10)